import os
import json
import esprima
import argparse
import zipfile

from extension_reader import ExtensionReader

def extract_features_from_source(source):
    def extract(tree):
        features = []
        for node in tree.body:
            if node.type == "FunctionDeclaration":
                features.append((node.type, node.id.name))
            elif type(node.body) == list:
                features.extend(extract(node))
        return features

    if type(source) is bytes:
        source = source.decode()
    try:
        tree = esprima.parse(source, {"tolerant": True})
    except (esprima.error_handler.Error, Exception) as e:
        print("Error", e)
        return []
    return extract(tree)


def extract_features(id_, extension_crx):
    with ExtensionReader(extension_crx) as extension:
        files = []
        # TODO: Expand to content scripts
        if "background" in extension.manifest and "scripts" in extension.manifest["background"]:
            files.extend(extension.manifest["background"]["scripts"])

        features = []
        for source in files:
            features.append(("BackgroundScript", source))
            features.extend(
                extract_features_from_source(extension.open(source.strip("/")).read())
            )

        name = extension.name()

    return {"id": id_, "name": name, "features": features}


def compare(src, tgts):
    src_result = extract_features(*src)

    tgt_results = []
    for tgt in tgts:
        tgt_result = extract_features(*tgt)
        num_matches = len(
            [f for f in src_result["features"] if f in tgt_result["features"]]
        )

        if src_result["features"]:
            tgt_result["similarity"] = round(
                num_matches / len(src_result["features"]), 2
            )
        else:
            tgt_result["similarity"] = None

        tgt_results.append(tgt_result)

    return {
        "source": src_result,
        "targets": tgt_results,
    }


def main(extensions, output):

    comparisons = []
    for ext in extensions:
        comparisons.append((ext, [e for e in extensions if e != ext]))

    result = []
    for src, tgts in comparisons:
        new_tgts = []
        for tgt in tgts:
            new_tgts.append((os.path.basename(tgt).strip(".crx"), tgt))

        result.append(compare((os.path.basename(src).strip(".crx"), src), new_tgts))

    if output:
        json.dump(result, open(output, "w"), indent="  ")
    else:
        print(json.dumps(result, indent="  "))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output")
    parser.add_argument("extensions", nargs="+")
    args = parser.parse_args()
    main(args.extensions, args.output)

