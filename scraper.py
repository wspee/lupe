import os
import requests
import argparse
import json
import chromedriver_py

from datetime import datetime
from selenium import webdriver

from extension_reader import ExtensionReader

keywords = ('adblocker',)
search_url = 'https://chrome.google.com/webstore/search/{keyword}?_category=extensions'
download_url = 'https://clients2.google.com/service/update2/crx?response=redirect&prodversion=88.0.4324.96&x=id%3D{id}%26installsource%3Dondemand%26uc&acceptformat=crx2,crx3'

PAGE_LOAD_TIMEOUT = 10
MAX_RESULTS = 50

class find_elements_number(object):
    '''Custom wait condition to wait for a certain number of elements'''
    def __init__(self, locator, number):
        self.locator = locator
        self.number = number

    def __call__(self, driver):
        elements = driver.find_elements(*self.locator)

        # See if we have found enough and return correct number of elements
        if len(elements) >= self.number:
            return elements[:self.number]

        # If not scroll to bottom of page to trigger loading next elements
        driver.execute_script("window.scrollTo(0, document.querySelector('div.F-k').scrollHeight);")

        return False

def extract_extension_ids(keywords):
    options = webdriver.chrome.options.Options()
    options.add_argument('--headless')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--no-sandbox')

    driver = webdriver.Chrome(executable_path=chromedriver_py.binary_path, options=options)

    results = {}

    for keyword in keywords:
        url = search_url.format(keyword=keyword)
        driver.get(url)

        wait = webdriver.support.ui.WebDriverWait(driver, PAGE_LOAD_TIMEOUT)

        for element in wait.until(find_elements_number((webdriver.common.by.By.CSS_SELECTOR, 'a.h-Ja-d-Ac'), MAX_RESULTS)):
            extension_url = element.get_attribute('href')
            results.setdefault(keyword, []).append(extension_url.split('/')[-1])

    return results

def download_extensions(extensions, target_dir):
    result = {}
    for keyword, extension_ids in extensions.items():
        for id_ in extension_ids:
            url = download_url.format(id=id_)
            crx = os.path.join(target_dir, '{}.crx'.format(id_))

            with requests.get(url, stream=True) as req:
                req.raise_for_status()
                with open(crx, 'wb') as f:
                    for chunk in req.iter_content(chunk_size=8192):
                        f.write(chunk)

            with ExtensionReader(crx) as extension:
                result.setdefault(keyword, []).append({
                    'id': id_,
                    'name': extension.name()
                })

    return result

def main(keywords, target_dir):
    extensions = extract_extension_ids(keywords)
    extensions = download_extensions(extensions, target_dir)

    result = {
        'date': datetime.now().isoformat(),
        'result': extensions,
    }

    with open(os.path.join(target_dir, 'result.json'), 'w') as f:
        json.dump(result, f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('keywords', nargs='+')
    parser.add_argument('output')
    args = parser.parse_args()
    main(args.keywords, args.output)
