import zipfile
import json

class ExtensionReader(object):
    def __init__(self, extension_crx):
        self.extension_crx = extension_crx

    def __enter__(self):
        self.extension = zipfile.ZipFile(self.extension_crx)
        self.manifest = json.load(self.extension.open('manifest.json'))
        return self

    def __exit__(self, type, value, traceback):
        self.extension.close()

    def _translate(self, msg_id, locales):
        if msg_id.startswith('__MSG_'):
            for locale in locales:
                try:
                    msgs_file = '_locales/{}/messages.json'.format(locale)
                    msgs = json.load(self.extension.open(msgs_file))
                except Exception:
                    continue
                else:
                    return msgs[msg_id.split('__MSG_')[-1].strip('__')]['message']
        return msg_id

    def open(self, *args, **kwargs):
        return self.extension.open(*args, **kwargs)

    def name(self):
        return self._translate(self.manifest['name'], ('en_US', 'en'))
